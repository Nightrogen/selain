using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Browser
{
    public partial class Form1 : Form
    {
            public static void EnsureBrowserEmulationEnabled(string exename = "Browser.exe", bool uninstall = false)    //yritetään käyttää uusinta asennettua IE versiota
            {
                try
                {
                    using (var rk = Registry.CurrentUser.OpenSubKey(
                        @"SOFTWARE\Microsoft\Internet Explorer\Main\FEATURE_BROWSER EMULATION", true)
                        )
                    {
                        if (!uninstall)
                        {
                            dynamic value = rk.GetValue("Browser");
                            if (value == null)
                                rk.SetValue(exename, (uint)11011, RegistryValueKind.DWord);
                        }
                        else
                            rk.DeleteValue(exename);
                    }
                }
                catch
                {
                }
            }
    public Form1()
        {
          InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;      //Joidenkin sivujen Javascript ei toimi, tällä päästään eroon error messageista
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)      //palaa edelliselle sivulle, jos mahdollista
        {
            if (webBrowser1.CanGoBack)
                {
                    webBrowser1.GoBack();
                }
        }

        private void button2_Click(object sender, EventArgs e)      //eteenpäin sivuhistoriassa
        {
            if (webBrowser1.CanGoForward)
            {
                webBrowser1.GoForward();
            }
        }

        private void button3_Click(object sender, EventArgs e)      //päivittää sivun
        {
            webBrowser1.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)      //siirtyy tekstikenttään syötetylle verkkosivulle
        {
            string WebPage = comboBox1.Text.Trim();
            webBrowser1.Navigate(WebPage);
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)     //Siirtyy kenttään syötetylle verkkosivulle enteriä painettaessa
        {
            if(e.KeyChar ==(char)13)
            {
                string WebPage = comboBox1.Text.Trim();
                webBrowser1.Navigate(WebPage);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}